### COLLABORATOR ####
www.collaboratorfsc.com

**Technologies Used**
Build using PHP, MySQL, and Laravel PHP Framework 
(For more information about Laravel visit https://laravel.com/)

**Design**
Project is designed using modern Model View Controller design pattern. This design pattern allows to develop web applications in object oriented programming way, where code is separated into 
* Models – back end code [logic of the system]
* Views – front end code [user interface]
* Controllers – back end code [connects and renders information to Views]

**Project File Structure** 
The following instructions explain folder location of source code developed for the project.

*Models:*
* app --> Models
* * Holds all model classes

*Controllers:*
* app --> Http --> Controllers
* * Holds all controller classes and routes class which specify all possible URL routes that user can navigate to.

*Views:*
* resources --> views
* * Holds all source code developed for front end user interface 
* public 
* * Holds all other elements of front end such as CSS, and JavaScript files, images used in the application